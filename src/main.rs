use std::ffi::IntoStringError;

use vulkano::VulkanLibrary;
use vulkano::instance::*;

fn main() {
    
    let vulkan = VulkanLibrary::new().expect("no local vulkan lib found");
    let instance = Instance::new(
        vulkan, 
        InstanceCreateInfo {
            flags: InstanceCreateFlags::ENUMERATE_PORTABILITY,
            ..Default::default()
        },
    )
    .expect("failed to create instance");

    let physical_devices = instance
        .enumerate_physical_devices()
        .expect("couldnt enumerate devices");

    for a in physical_devices {
        println!("{:?}", a.properties().device_name)
    }

    println!("Hello, world!");

}
